package view;
import model.*;
import java.util.*;
import javax.swing.*;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View {

	public View() {
		JFrame frame = new JFrame("Fechete Dragos Ioan - tema 2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1100, 600);
		frame.setContentPane(panel);
		
		panel.setLayout(new BorderLayout());
		panel.add(inputs, BorderLayout.LINE_START);
		panel.add(queues, BorderLayout.CENTER);
		panel.add(text, BorderLayout.LINE_END);
		
		inputs.setLayout(new GridLayout(5,2));
		inputs.setPreferredSize(new Dimension(400, 400));
		minServingTimeField.setPreferredSize(new Dimension(100,20));
		minServingTimeField.setMaximumSize(minServingTimeField.getPreferredSize());
		maxServingTimeField.setPreferredSize(new Dimension(100,20));
		maxServingTimeField.setMaximumSize(minServingTimeField.getPreferredSize());
		minArrivalTimeField.setPreferredSize(new Dimension(100,20));
		minArrivalTimeField.setMaximumSize(minArrivalTimeField.getPreferredSize());
		maxArrivalTimeField.setPreferredSize(new Dimension(100,20));
		maxArrivalTimeField.setMaximumSize(maxArrivalTimeField.getPreferredSize());
		intervalSimField.setPreferredSize(new Dimension(100,20));
		intervalSimField.setMaximumSize(intervalSimField.getPreferredSize());
		nrCozi.setPreferredSize(new Dimension(100,20));
		nrCozi.setMaximumSize(nrCozi.getPreferredSize());
		loggerField.setPreferredSize(new Dimension(300,600));
		loggerField.setBackground(Color.white);
		loggerField.setEditable(false);
		minServingPanel.add(Box.createRigidArea(new Dimension(0,108)));
		minServingPanel.add(minServingTimeField);
		maxServingPanel.add(Box.createRigidArea(new Dimension(0,108)));
		maxServingPanel.add(maxServingTimeField);
		minArrivalPanel.add(Box.createRigidArea(new Dimension(0,108)));
		minArrivalPanel.add(minArrivalTimeField);
		maxArrivalPanel.add(Box.createRigidArea(new Dimension(0,108)));
		maxArrivalPanel.add(maxArrivalTimeField);
		intPanel.add(Box.createRigidArea(new Dimension(0,108)));
		intPanel.add(intervalSimField);
		intPanel.add(Box.createRigidArea(new Dimension(0,108)));
		coziPanel.add(Box.createRigidArea(new Dimension(0,108)));
		coziPanel.add(nrCozi);
		startPanel.add(start);
		inputs.add(minServingTimeLabel);
		inputs.add(minServingPanel);
		inputs.add(maxServingPanel);
		inputs.add(minArrivalTimeLabel);
		inputs.add(minArrivalPanel);
		inputs.add(maxArrivalPanel);
		inputs.add(intervalSimLabel);
		inputs.add(intervalSimField);
		inputs.add(new JPanel());
		inputs.add(cozi);
		inputs.add(coziPanel);
		inputs.add(new JPanel());
		inputs.add(new JPanel());
		inputs.add(new JPanel());
		inputs.add(startPanel);
		
		queues.setLayout(new GridLayout(0,2));
		queues.setBackground(Color.lightGray);
		queues.setMaximumSize(new Dimension(100,400));
		
		text.setLayout(new FlowLayout());
//		text.setBackground(Color.white);
		text.add(loggerField);
		text.setPreferredSize(new Dimension(300,500));
		
		frame.setVisible(true);
		
		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				minServTime = Integer.parseInt(minServingTimeField.getText());
				maxServTime = Integer.parseInt(maxServingTimeField.getText());
				minArrivTime = Integer.parseInt(minArrivalTimeField.getText()); 
				maxArrivTime = Integer.parseInt(maxArrivalTimeField.getText());
				intSim = Integer.parseInt(intervalSimField.getText());
				queuesNr = Integer.parseInt(nrCozi.getText());
				addQueues(queuesNr);
				s = new Simulation(v, 10, minServTime, maxServTime, minArrivTime, maxArrivTime, intSim, queuesNr);
				Thread thread = new Thread(s);
				thread.start();
			}
		});
		
	}
		
	public void addQueues(int nrCozi) {
		for(int i = 0; i < nrCozi; i++) {
			queueField.add(new JTextField(10));
			queueLabel.add(new JLabel("Queue " + (i+1)));
			queues.add(queueLabel.get(i));
			queues.add(queueField.get(i));
		}
		queues.revalidate();
		queues.repaint();
	}
	
	public void loggerPrint(String str) {
		this.loggerField.setText("" + this.loggerField.getText() + "\n" + str);
	}
	
	public int minServTime, maxServTime, minArrivTime, maxArrivTime, intSim, queuesNr;
	
	public JButton start = new JButton(" Start ");
	public JLabel minServingTimeLabel = new JLabel(" Timp de servire:");
	public JLabel minArrivalTimeLabel = new JLabel(" Timp de sosire:");
	public JLabel intervalSimLabel    = new JLabel(" Interval de simulare:");
	public JLabel cozi 			      = new JLabel(" Nr de cozi:");
	public JTextField minServingTimeField = new JTextField();
	public JTextField maxServingTimeField = new JTextField();
	public JTextField minArrivalTimeField = new JTextField();
	public JTextField maxArrivalTimeField = new JTextField();
	public JTextField intervalSimField	  = new JTextField();
	public JTextField nrCozi			  = new JTextField();
	public JTextArea loggerField		  = new JTextArea();
	public ArrayList<JTextField> queueField  = new ArrayList<JTextField>();
	public ArrayList<JLabel> queueLabel 	 = new ArrayList<JLabel>();
	public JPanel panel  = new JPanel();
	public JPanel inputs = new JPanel();
	public JPanel queues = new JPanel();
	public JPanel text   = new JPanel();
	public JPanel minServingPanel = new JPanel();
	public JPanel maxServingPanel = new JPanel();
	public JPanel minArrivalPanel = new JPanel();
	public JPanel maxArrivalPanel = new JPanel();
	public JPanel intPanel  	  = new JPanel();
	public JPanel coziPanel	   	  = new JPanel();
	public JPanel startPanel 	  = new JPanel();
	public Simulation s;
	public View v = this;
}
