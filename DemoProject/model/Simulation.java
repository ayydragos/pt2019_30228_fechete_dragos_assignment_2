package model;


import java.util.*;
import view.View;

public class Simulation implements Runnable {
	private ArrayList<Coada> cozi = new ArrayList<Coada>();
	private ArrayList<Client> clienti = new ArrayList<Client>();
	private int nrClienti = 1;
	private int minServingTime = 1;
	private int maxServingTime = 5;
	private	int minArrivingTime = 1;
	private int maxArrivingTime = 20;
	private int intSimulare = 12;
	private int queuesNr = 5;
	public View v;
	
	public ArrayList<Client> placeInQueue(int time) {
		ArrayList<Client> clientiSimultan = new ArrayList<Client>();
		for(Client c: clienti) {
			if(c.getArrivalTime() == time) {
				Client aux = c;
				clientiSimultan.add(aux);
			}
		}
		for(Client c: clientiSimultan) {
			clienti.remove(c);
		}
		return clientiSimultan;
	}
	
	@Override
	public void run() {
		int time = 0;
		while(true) {
			try {
				ArrayList<Client> listaClienti = placeInQueue(time);
				for(Client aux: listaClienti) {
					int coadaMinima = 0;
					for(int i = 0 ; i < cozi.size(); i++) {
						if(cozi.get(i).nrClienti() <= cozi.get(coadaMinima).nrClienti()) {
							coadaMinima = i;
						}
					}
					
					cozi.get(coadaMinima).adaugareClient(aux);
					v.loggerPrint("Client(id: " + aux.getNr() + ", timp: "+ aux.getServingTime() + ") adaugat la coada " + (coadaMinima+1));
					v.queueField.get(coadaMinima).setText("" + v.queueField.get(coadaMinima).getText() + " client ");
				}
					
//				for(int i = 0; i < queuesNr; i++) {
//					System.out.print(coada.nrClienti() + " ");
//					v.queueField.get(i).setText(" " + v.queueField.get(i).getText() + " client ");
//				}
				
				System.out.println(" ");
				if(time == intSimulare) {
					v.loggerPrint("sfarsit simulare");
					break;
				}
				
				Thread.sleep(1000);
				time++;	
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void generateClients() {
		for(int i = 1; i <= nrClienti; i++) {
			Random rand = new Random();
			int serviceTime = rand.nextInt(maxServingTime - minServingTime + 1) + minServingTime;  	
			int arrivalTime = rand.nextInt(maxArrivingTime - minArrivingTime + 1) + minArrivingTime;
			
			Client c = new Client(arrivalTime,serviceTime,i);
			clienti.add(c);
		}
		Collections.sort(clienti);
	}	
	
	private void generateQueues() {
		for(int i = 0; i < queuesNr; i++) {
			cozi.add(new Coada(i+1, v));
			Thread t = new Thread(cozi.get(i));
			t.start();
		}
	}
	
	public Simulation(View v, int nrClienti, int minServingTime, int maxServingTime, int minArrivingTime, int maxArrivingTime, int intSimulare, int queuesNr) {
		this.v = v;
		this.nrClienti = nrClienti;
		this.minServingTime = minServingTime;
		this.maxServingTime = maxServingTime;
		this.minArrivingTime = minArrivingTime;
		this.maxArrivingTime = maxArrivingTime;
		this.intSimulare = intSimulare;
		this.queuesNr = queuesNr;
		generateClients();
		generateQueues();
	}
}
