package model;

public class Client implements Comparable<Client> {
	
	public Client() {
		this(0,0,0);
	}
	
	public Client(int a, int s, int n) {
		arrivalTime = a;
		servingTime = s;
		nr = n;
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getServingTime() {
		return servingTime;
	}

	public int getNr() {
		return nr;
	}
	
	public int getFinalTime() {
		return arrivalTime + servingTime;
	}
	
	public String toString(int timp, int id) {
		String cl = "";
		cl += timp;
		cl = "Client(id: " + (id+1) + ", timp: " + cl + ") ";
		return cl;
	}
	
	private int arrivalTime;
	private int servingTime;
	private int nr;
	
	public int compareTo(Client c) {
		return (this.getArrivalTime() - c.getArrivalTime());
	}
}
	
 