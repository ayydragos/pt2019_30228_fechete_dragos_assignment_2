package model;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

import view.*;

public class Coada implements Runnable {
	
	public Coada(View v) {
		this(0, v);
	}

	public Coada(int n, View v) {
		clienti.clear();
		waitingTime.set(0);
		id = n;		
	}
	
	public int nrClienti() {
		return clienti.size();
	}
	
	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		String queue = "";
		int time = 0;
		int i;
		
		for(i = clienti.size() - 1; i >= 0; i--) {
			time += clienti.get(i).getServingTime();
			queue += clienti.get(i).toString(time, i) + " ";
		}
		return queue;
	}

	public void adaugareClient(Client client) {
		try {
			clienti.add(client);
			waitingTime.set(waitingTime.get() + client.getFinalTime() - client.getArrivalTime());
		}
		catch(Exception e) {
//			e.printStackTrace();
		}
	}
	
	public void plecareClient() {
		try {
			Client client = new Client();
				client = clienti.remove();
				Thread.sleep(client.getServingTime() * 1000);
				waitingTime.set(waitingTime.get() + client.getArrivalTime() - client.getFinalTime());
				
		}
		catch(Exception e) {
//			e.printStackTrace();
		}
	}
	
	public void run() {
		while(true) {
			try {
				plecareClient();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private LinkedList<Client> clienti = new LinkedList<Client>();
	private AtomicInteger waitingTime = new AtomicInteger(0);
	private int id;	
	public View v;
}
	
